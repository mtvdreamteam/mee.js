var versionNumber = "1.4";

//Parameters for mee.js functions
var HORIZONTAL = "HORIZONTAL";
var VERTICAL = "VERTICAL";
var ARRAY = "ARRAY";
var OBJECT = "OBJECT";

var meejs = {};

window.onload = function() {
  //This will display when mee.js is loaded
  console.log("------------------------------")
  console.log(" mee.js version", versionNumber, "loaded");
  console.log("------------------------------")
};

  /*
  * FRONT-END
  */


meejs.dottedLine = function(direction, value, distance, pointRadius){
  if (isNaN(value) == false && isNaN(distance) == false && isNaN(pointRadius) == false){
    var canvas = document.getElementById('defaultCanvas0');
    if (direction == HORIZONTAL){
      for (var i = 0; i < canvas.width / distance + 1; i++) {
        rect(i * distance - pointRadius, value - pointRadius, pointRadius * 2, pointRadius * 2)
      }
    }else if (direction == VERTICAL){
      for (var i = 0; i < canvas.height / distance + 1; i++) {
        rect(value - pointRadius, i * distance - pointRadius, pointRadius * 2, pointRadius * 2)
      }
    }else{
      throw new Error("Invalid direction for function 'dottedLine'.");
    }
  }else{
    throw new Error("Invalid agruments for function 'dottedLine'.");
  }
}

meejs.grid = function(boxWidth, boxHeigth, lineWeight){
  if (isNaN(boxWidth) == false && isNaN(boxHeigth) == false && isNaN(lineWeight) == false){
    var canvas = document.getElementById('defaultCanvas0');
    var verBoxes = canvas.height / boxWidth;
    var horBoxes = canvas.width / boxHeigth;

    for (var i = 0; i < horBoxes; i++) {
      push();
      strokeWeight(lineWeight);
      line(0, i * boxHeigth, canvas.width, i * boxHeigth);
      pop();
    }
    for (var i = 0; i < verBoxes; i++) {
      push();
      strokeWeight(lineWeight);
      line(i * boxWidth, 0, i * boxWidth, canvas.height);
      pop();
    }
  }else{
    throw new Error("Invalid agruments for function 'grid'.");
  }
}

meejs.dotBoard = function(distance, dotSize){
  if (isNaN(distance) == false && isNaN(dotSize) == false){
    var canvas = document.getElementById('defaultCanvas0');

    for (var i = 1; i < canvas.width / distance; i++) {
      for (var j = 1; j < canvas.height / distance; j++) {
        ellipse(i * distance, j * distance, dotSize, dotSize);
      }
    }
  }else{
    throw new Error("Invalid agruments for function 'dotBoard'.");
  }
}

meejs.dottedCircle = function(x, y, radius, population, dotSize){
  for (var i = 0; i < population; i++) {
    var objX = x + radius * cos((360 / population * i) * Math.PI / 180);
    var objY = y + radius * sin((360 / population * i) * Math.PI / 180);

    ellipse(objX, objY, dotSize, dotSize);
  }
}


/*
* BACK-END
*/

meejs.requestText = function(url, callback){
  if (isNaN(url) == true){
    var request = new XMLHttpRequest();
    request.open('GET', url,  true);
    request.send(null);
    request.onreadystatechange = function () {
      if (request.readyState === 4 && request.status === 200) {
        var type = request.getResponseHeader('Content-Type');
        if (type.indexOf("text") !== 1) {
          callback(request.responseText);
        }
      }
    }
  }else{
    throw new Error("Invalid agruments for function 'requestText'.");
  }
}

meejs.randomBetween = function(min, max){
  if (isNaN(min) == false && isNaN(max) == false){
    return Math.floor(Math.random() * max) + min;
  }else{
    throw new Error("Invalid agruments for function 'randomBetween'.");
  }
}

meejs.getLocatonOnCircle = function(originX, originY, radius, degrees){
  if (isNaN(originX) == false && isNaN(originY) == false && isNaN(radius) == false && isNaN(degrees) == false){
    degrees = degrees - 90;
    var returnObject = {};
    returnObject.x = originX + radius * cos(degrees * Math.PI / 180);
    returnObject.y = originY + radius * sin(degrees * Math.PI / 180);
    return returnObject;
  }else{
    throw new Error("Invalid agruments for function 'getLocatonOnCircle'.");
  }
}

meejs.findLast = function(array, type, header, value){
  if (array.constructor === Array){
    if (type == ARRAY){
      if (isNaN(header) == false){
        for (var i = array.length - 1; i > -1; i--){
          if (array[i][header] == value){
            return i
            break;
          }
        }
      }else{
        throw new Error("Invalid agruments for function 'findLast'.");
      }
    }else if (type == OBJECT){
      for (var i = array.length - 1; i > -1; i--){
        if (array[i][header] == value) {
          return i
          break;
        }
      }
    }else{
      throw new Error("Invalid agruments for function 'findLast'.");
    }
  }else{
    throw new Error("Invalid agruments for function 'findLast'.");
  }
}

meejs.findFirst = function(array, type, header, value){
  if (array.constructor === Array){
    if (type == ARRAY){
      if (isNaN(header) == false){
        for (var i = 0; i < array.length; i++) {
          if (array[i][header] == value){
            return i
            break;
          }
        }
      }else{
        throw new Error("Invalid agruments for function 'findLast'.");
      }
    }else if (type == OBJECT){
      for (var i = 0; i < array.length; i++) {
        if (array[i][header] == value) {
          return i
          break;
        }
      }
    }else{
      throw new Error("Invalid agruments for function 'findLast'.");
    }
  }else{
    throw new Error("Invalid agruments for function 'findLast'.");
  }
}

meejs.findAll = function(array, type, header, value){
  if (array.constructor === Array){
    if (type == ARRAY){
      if (isNaN(header) == false){
        var returnArray = [];
        for (var i = 0; i < array.length; i++) {
          if (array[i][header] == value){
            returnArray.push(i);
          }
        }
        return returnArray;
      }else{
        throw new Error("Invalid agruments for function 'findLast'.");
      }
    }else if (type == OBJECT){
      var returnArray = [];
      for (var i = 0; i < array.length; i++) {
        if (array[i][header] == value) {
          returnArray.push(i);
        }
      }
      return returnArray;
    }else{
      throw new Error("Invalid agruments for function 'findLast'.");
    }
  }else{
    throw new Error("Invalid agruments for function 'findLast'.");
  }
}
//©Mees van der Wijk
